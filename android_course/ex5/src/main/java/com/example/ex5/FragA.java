package com.example.ex5;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FragA extends Fragment implements OnClickListener {
    ClickHandler listener;
    public EditText firstOp;
    public EditText secondOp;
    public Button mul;
    public Button div;
    public Button dec;
    public Button add;
    public Button clear;
    // This is the cookbook 1 method used to disable or enable op buttons
    private final TextWatcher listenOnChanges = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            enableAll();
        }

        public void afterTextChanged(Editable s) {
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                disableAll();
            } else {
                enableAll();
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.frag_a, container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        add = view.findViewById(R.id.button1);
        dec = view.findViewById(R.id.button2);
        div = view.findViewById(R.id.button3);
        mul = view.findViewById(R.id.button4);
        add.setOnClickListener(this);
        dec.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);
        view.findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstOp.setText("");
                secondOp.setText("");
            }
        });
        firstOp = view.findViewById(R.id.firstop);
        secondOp = view.findViewById(R.id.secondop);
        firstOp.addTextChangedListener(listenOnChanges);
        secondOp.addTextChangedListener(listenOnChanges);
        disableAll();
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        try{
            this.listener = (ClickHandler)getActivity();
        }catch(ClassCastException e){
            throw new ClassCastException("the class " +
                    getActivity().getClass().getName() +
                    " must implements the interface 'ClickHandler'");
        }
        if (savedInstanceState!=null && savedInstanceState.get("OP1")!=null) {firstOp.setText(savedInstanceState.get("OP1").toString());}
        if (savedInstanceState!=null && savedInstanceState.get("OP2")!=null) {secondOp.setText(savedInstanceState.get("OP2").toString());}
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onClick(View v) {
        listener.OnClickEvent(v);
    }

    public interface ClickHandler{
        void OnClickEvent(View v);
    }


    public void disableAll() {
        add.setEnabled(false);
        dec.setEnabled(false);
        div.setEnabled(false);
        mul.setEnabled(false);

    }

    public void enableAll() {
        add.setEnabled(true);
        dec.setEnabled(true);
        div.setEnabled(true);
        mul.setEnabled(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("logme555","onSaveInstanceState func called in FRAG-A");
        if (!firstOp.getText().toString().isEmpty()) {
            outState.putFloat("OP1", Float.valueOf(firstOp.getText().toString()));
        }
        if (!secondOp.getText().toString().isEmpty()) {
            outState.putFloat("OP2", Float.valueOf(secondOp.getText().toString()));
        }

    }
}
