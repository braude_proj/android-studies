package com.example.ex5;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Arrays;


public class FragB extends Fragment implements SeekBar.OnSeekBarChangeListener {
    DataReporter listner;
    public TextView result;
    public SeekBar seekbar;
    public int decimal_place = 2;
    float res;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.frag_b, container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.seekbar = view.findViewById(R.id.seekBar);
        this.result = view.findViewById(R.id.result);
        seekbar.setOnSeekBarChangeListener(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        try{
            this.listner = (DataReporter)getActivity();
        }catch(ClassCastException e){
            throw new ClassCastException("the class " +
                    getActivity().getClass().getName() +
                    " must implements the interface 'DataReporter'");
        }
        this.result.setText(listner.getData());
        if (savedInstanceState!=null && savedInstanceState.get("RES")!=null) {result.setText(savedInstanceState.get("RES").toString());}
        super.onActivityCreated(savedInstanceState);
    }

    public void formulaChanged(String newformula){
        String resString = String.format("%." + decimal_place + "f", res);
        result.setText(resString);
        String final_result = newformula  + resString;
        this.result.setText(final_result);
    }

    public void updateResult(float new_result) {
        res = new_result;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        decimal_place = progress;
        if (result.getText().toString().isEmpty()) return;
        formulaChanged(listner.getData());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface DataReporter{
        String getData();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("logme555","onSaveInstanceState func called in FRAG-B");
        if (!result.getText().toString().isEmpty()) {outState.putFloat("RES",Float.valueOf(result.getText().toString()));}
    }

}
