package com.example.ex5;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.content.res.Configuration;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements FragA.ClickHandler, FragB.DataReporter{

    private String calculated_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null ) {
            this.calculated_string = savedInstanceState.getString("calculated_string");
        }
        //I just put the fragA in the port xml
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
				FragmentManager mFragmentManager = getFragmentManager();
	          // However if we are being restored from a previous state, then we don't
	          // need to do anything and should return or we could end up with overlapping Fragments
	     //     if (savedInstanceState != null && mFragmentManager.findFragmentByTag("AAA")!=null){
	          	if (savedInstanceState != null &&
						mFragmentManager.findFragmentById(R.id.fragA)!=null)
					  return;
	          mFragmentManager.beginTransaction().
					  add(R.id.fragContainer, new FragA()).
					  commit();
		}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    protected void onPause() {

		FragmentManager fm = getFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
		int cnt = fm.getBackStackEntryCount();

        super.onPause();
    }

    @Override
    public void OnClickEvent(View view) {
        FragB fragB;
        FragA fragA;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            fragA = (FragA) getFragmentManager().findFragmentById(R.id.fragA);
        } else {
            fragA = (FragA) getFragmentManager().findFragmentById(R.id.fragContainer);
        }
        float res = 0;
        if(fragA==null) {
            Log.d("calculator","Could not find FragA!!!!");
            //TODO: handle this
            return;
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            fragB = (FragB) getFragmentManager().findFragmentById(R.id.fragB);
        else //I am in portrait
        {
            fragB = new FragB();
            getFragmentManager().beginTransaction().add(R.id.fragContainer, fragB).//add on top of the static fragment
                    addToBackStack("BBB").//cause the back button scrolling through the loaded fragments
                    commit();
            getFragmentManager().executePendingTransactions();
        }
        Log.d("calculator","got into calc func with view:" + view.toString());
        try {
            String op = ((Button)view).getText().toString();
            if (fragA.firstOp.getText().toString().isEmpty() || fragA.secondOp.getText().toString().isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Must Fill Both Operands!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
            String firstOp = fragA.firstOp.getText().toString();
            String secondOp = fragA.secondOp.getText().toString();
            Float first = Float.valueOf(firstOp);
            Float second = Float.valueOf(secondOp);
            //String.format("%.2f", floatValue);
            switch (op) {
                case "+":
                    res = first + second;
                    break;
                case "-":
                    res = first - second;
                    break;
                case "*":
                    res = first * second;
                    break;
                case "/":
                    if (second == 0) {
                        Context context = getApplicationContext();
                        CharSequence text = "Don't Divide By Zero!!!";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        fragB.updateResult(0);
                        fragB.formulaChanged(" Divide By Zero Error");
                        return;
                    } else {
                        res = first / second;
                    }
                    break;
            }
            fragB.updateResult(res);
            calculated_string = firstOp + " " + op + " " + secondOp + " = ";
            fragB.formulaChanged(calculated_string);
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "SOME ERROR!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            fragB.result.setText("ERROR");
        }



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("calculated_string", this.calculated_string);
        super.onSaveInstanceState(outState);
    }

    @Override
    public String getData() {
        return this.calculated_string;
    }
}
