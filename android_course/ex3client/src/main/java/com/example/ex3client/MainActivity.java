package com.example.ex3client;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("info_tibi","on create!!");
    }

    public void callb(View view) {
        Log.d("info_tibi","callb!!");
        EditText phone_text;
        phone_text = findViewById(R.id.editText);
        String phone_number = phone_text.getText().toString();

        Uri number = Uri.parse("tel:" + phone_number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        if (callIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(callIntent);
        } else {
            Toast.makeText(MainActivity.this, "NO DIALER FOUND!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void surfb(View view) {
        Log.d("info_tibi","surfb!!");
        EditText text;
        text = findViewById(R.id.webedittext);
        String web_addr = text.getText().toString();

        Uri web = Uri.parse(web_addr);
        Intent surfIntent = new Intent(Intent.ACTION_VIEW, web);
        if (surfIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(surfIntent);
        } else {
            Toast.makeText(MainActivity.this, "NO WEB BROWSER FOUND!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void emailb(View view) {
        Log.d("info_tibi","emailb!!");
        EditText text;
        text = findViewById(R.id.emaileditText3);
        String email_addr = text.getText().toString();

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // The intent does not have a URI, so declare the "text/plain" MIME type
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email_addr}); // recipients
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text");
        // You can also attach multiple items by passing an ArrayList of Uris
        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(emailIntent);
        } else {
            Toast.makeText(MainActivity.this, "NO EMAIL SERVICE FOUND!",
                    Toast.LENGTH_LONG).show();
        }

    }

    public void regb(View view) {
        Log.d("info_tibi","regb!!");
        Intent regIntent = new Intent( "com.action.register");
        if (regIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(regIntent,1);
        } else {
            Toast.makeText(MainActivity.this, "NO REGISTRATION APP FOUND!!",
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data==null) {
            Context context = getApplicationContext();
            CharSequence t = "operation canceled";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, t, duration);
            toast.show();
            return;
        }
        Log.d("info_tibi","returned from register activity!! got:"+data.getData().toString());
        String result = data.getData().toString();
        String fname = result.split(" ")[0];
        String lname = result.split(" ")[1];
        String gender = result.split(" ")[2];
        if (gender.equals("Male")) gender = "Mr.";
        else gender = "Ms.";
        EditText et = findViewById(R.id.reg);

        et.setText("Welcome back " + gender + " " + fname + " " + lname + " ");

    }
}
