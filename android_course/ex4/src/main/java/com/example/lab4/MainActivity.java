package com.example.lab4;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    public EditText firstOp;
    public EditText secondOp;
    public TextView result;
    public Button mul;
    public Button div;
    public Button dec;
    public Button add;
    public Button clear;
    public SeekBar seekbar;
    public int decimal_place;
    public Float res;

    @Override
    protected void onRestoreInstanceState(Bundle saved){
        Log.i("logme555","onRestoreInstanceState func called");
        if (saved.get("OP1")!=null) {firstOp.setText(saved.get("OP1").toString());}
        if (saved.get("OP2")!=null) {secondOp.setText(saved.get("OP2").toString());}
        if (saved.get("RES")!=null) {result.setText(saved.get("RES").toString());}
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("logme555","onStart func called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("logme555","onStop func called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("logme555","onDestroy func called");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("logme555","onSaveInstanceState func called");
        if (!firstOp.getText().toString().isEmpty()) {outState.putFloat("OP1",Float.valueOf(firstOp.getText().toString()));}
        if (!secondOp.getText().toString().isEmpty()) {outState.putFloat("OP2",Float.valueOf(secondOp.getText().toString()));}
        if (!result.getText().toString().isEmpty()) {outState.putFloat("RES",Float.valueOf(result.getText().toString()));}
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("logme555","onPause func called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("logme555","onResume func called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("logme555", "onRestart func called");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = new Float(0.0);
        setContentView(R.layout.activity_main);
        Log.i("logme555","onCreate func called");
        firstOp = findViewById(R.id.firstop);
        secondOp = findViewById(R.id.secondop);
        result = findViewById(R.id.textView2);
        add = findViewById(R.id.button1);
        dec = findViewById(R.id.button2);
        div = findViewById(R.id.button3);
        mul = findViewById(R.id.button4);
        //this is using cookbook 3 anonym implementation
        findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstOp.setText("");
                secondOp.setText("");
            }
        });
        decimal_place = 2;
        disableAll();
        firstOp.addTextChangedListener(listenOnChanges);
        secondOp.addTextChangedListener(listenOnChanges);


        //used include from xml when oriantation is portrait and using inflater when landscape
        if (this.getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT ){
            seekbar = findViewById(R.id.seekBar);
        } else {
            ViewGroup parentLayout = findViewById(R.id.MainLayout);
            View child = getLayoutInflater().inflate(R.layout.seekbarlay, parentLayout, false);
            RelativeLayout.LayoutParams rlp =new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            rlp.addRule(RelativeLayout.BELOW, R.id.aboveseekbar);
            //rlp.setMargins(0,dp2px(100),0,0);
            parentLayout.addView(child,rlp);
            seekbar= child.findViewById(R.id.seekBar);
        }

        // using cookbook #4 implementing within this class (saves some memory) we dont need an object
        seekbar.setOnSeekBarChangeListener(this);
    }

    public int dp2px(int dp){
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    // This is the cookbook 1 method used to disable or enable op buttons
    private final TextWatcher listenOnChanges = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            enableAll();
        }

        public void afterTextChanged(Editable s) {
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                disableAll();
            } else {
                enableAll();
            }
        }
    };

    public void calc(View view) {
        Log.d("calculator","got into calc func with view:" + view.toString());
        try {
            String op = ((Button)view).getText().toString();
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Must Fill Both Operands!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
            Float first = Float.valueOf(firstOp.getText().toString());
            Float second = Float.valueOf(secondOp.getText().toString());
            //String.format("%.2f", floatValue);
            if(op.equals("+")) {
                res = first + second;
            }else if(op.equals("-")) {
                res = first - second;
            } else if (op.equals("*")) {
                res = first * second;
            } else if (op.equals("/")) {
                if (second==0) {
                    Context context = getApplicationContext();
                    CharSequence text = "Don't Divide By Zero!!!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                } else {
                    res = first / second;
                }
            }
            String resString = String.format("%." + decimal_place + "f\n", res);
            result.setText(resString);
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "SOME ERROR!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            result.setText("ERROR");
        }
    }

    public void disableAll() {
        add.setEnabled(false);
        dec.setEnabled(false);
        div.setEnabled(false);
        mul.setEnabled(false);

    }

    public void enableAll() {
        add.setEnabled(true);
        dec.setEnabled(true);
        div.setEnabled(true);
        mul.setEnabled(true);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        decimal_place = progress;
        if (result.getText().toString().isEmpty()) return;
        String resString = String.format("%." + decimal_place + "f\n", res);
        result.setText(resString);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
