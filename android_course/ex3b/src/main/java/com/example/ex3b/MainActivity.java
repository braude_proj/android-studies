package com.example.ex3b;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final String REGISTER = "com.example.lab3a.regActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("info_tibi","on create!!");

    }


    public void register_clicked(View view) {
        Log.d("info_tibi","register button clicked!");
        Intent intent = new Intent(this,regActivity.class);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("info_tibi","returned from register activity!! got:"+data.getData().toString());
        String result = data.getData().toString();
        String fname = result.split(" ")[0];
        String lname = result.split(" ")[1];
        String gender = result.split(" ")[2];
        if (gender.equals("Male")) gender = "Mr.";
        else gender = "Ms.";
        // TODO: change button text to "again..."
        Button b = findViewById(R.id.regbutton);
        b.setText("Again...");

        // TODO: write on screen the registered name and gender
        TextView tv = findViewById(R.id.textView);
        tv.setText("Welcome back " + gender + " " + fname + " " + lname + " ");

    }
}
