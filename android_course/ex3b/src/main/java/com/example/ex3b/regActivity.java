package com.example.ex3b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class regActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


    public void return_Result(View view) {
        Intent data = new Intent();
        String text;
        TextInputEditText fnameT= findViewById(R.id.first_name);
        TextInputEditText lnameT= findViewById(R.id.last_name);
        RadioGroup rb = findViewById(R.id.rgroup);
        int radioButtonID = rb.getCheckedRadioButtonId();
        View radioButton = rb.findViewById(radioButtonID);
        int idx = rb.indexOfChild(radioButton);
        RadioButton r = (RadioButton) rb.getChildAt(idx);
        if (fnameT.getText()==null || lnameT.getText()==null || r==null) {
            Context context = getApplicationContext();
            CharSequence t = "Please fill all fields and choose gender!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, t, duration);
            toast.show();
            return;
        } else {
            text = fnameT.getText().toString();
            text = text.concat(" " + lnameT.getText().toString());
            text = text.concat(" " + r.getText().toString());
        }
        //---set the data to pass back---
        data.setData(Uri.parse(text));
        setResult(RESULT_OK, data);
        //---close the activity---
        finish();
    }
}
