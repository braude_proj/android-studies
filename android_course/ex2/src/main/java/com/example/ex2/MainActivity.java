package com.example.ex2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public EditText firstOp;
    public EditText secondOp;
    public TextView result;

    @Override
    protected void onRestoreInstanceState(Bundle saved){
        Log.i("logme555","onRestoreInstanceState func called");
        if (saved.get("OP1")!=null) {firstOp.setText(saved.get("OP1").toString());}
        if (saved.get("OP2")!=null) {secondOp.setText(saved.get("OP2").toString());}
        if (saved.get("RES")!=null) {result.setText(saved.get("RES").toString());}
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("logme555","onStart func called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("logme555","onStop func called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("logme555","onDestroy func called");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("logme555","onSaveInstanceState func called");
        if (!firstOp.getText().toString().isEmpty()) {outState.putFloat("OP1",Float.valueOf(firstOp.getText().toString()));}
        if (!secondOp.getText().toString().isEmpty()) {outState.putFloat("OP2",Float.valueOf(secondOp.getText().toString()));}
        if (!result.getText().toString().isEmpty()) {outState.putFloat("RES",Float.valueOf(result.getText().toString()));}
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("logme555","onPause func called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("logme555","onResume func called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("logme555", "onRestart func called");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("logme555","onCreate func called");
        firstOp = findViewById(R.id.firstop);
        secondOp = findViewById(R.id.secondop);
        result = findViewById(R.id.textView2);
    }

    public void calc(View view) {
        Log.d("calculator","got into calc func with view:" + view.toString());
        try {
            String op = ((Button)view).getText().toString();
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Must Fill Both Operands!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
            Float first = Float.valueOf(firstOp.getText().toString());
            Float second = Float.valueOf(secondOp.getText().toString());
            Float res;
            if(op.equals("+")) {
                res = first + second;
                result.setText(res.toString());
            }else if(op.equals("-")) {
                res = first - second;
                result.setText(res.toString());
            } else if (op.equals("*")) {
                res = first * second;
                result.setText(res.toString());
            } else if (op.equals("/")) {
                if (second==0) {
                    Context context = getApplicationContext();
                    CharSequence text = "Don't Divide By Zero!!!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    result.setText("Infinity");
                    return;
                } else {
                    res = first / second;
                    result.setText(res.toString());
                }
            }
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "SOME ERROR!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            result.setText("ERROR");
        }
    }
}
