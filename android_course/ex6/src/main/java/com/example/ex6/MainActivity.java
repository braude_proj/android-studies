package com.example.ex6;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MainActivity extends AppCompatActivity implements MyDialogFragment.DialogListener {

    public EditText firstOp;
    public EditText secondOp;
    public TextView result;
    public Button mul;
    public Button div;
    public Button dec;
    public Button add;
    public Button clear;
    public SeekBar seekbar;
    private int decimal_place;
    public Float res;

    @Override
    protected void onRestoreInstanceState(Bundle saved){
        Log.i("logme555","onRestoreInstanceState func called");
        if (saved.get("OP1")!=null) {firstOp.setText(saved.get("OP1").toString());}
        if (saved.get("OP2")!=null) {secondOp.setText(saved.get("OP2").toString());}
        if (saved.get("RES")!=null) {result.setText(saved.get("RES").toString());}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.exit:
                exitApp();
                return true;
            case R.id.settings:
                showSeekBark();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSeekBark() {
        MyDialogFragment dialogFragment = new MyDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", "Decimal Value");
        bundle.putInt("decimal_val", decimal_place);
        dialogFragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        dialogFragment.show(ft, "dialog");
    }

    private void exitApp() {


        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Closing the Application");
        adb.setMessage("are you sure?");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        adb.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adb.show();


    }


    public void disableAll() {
        add.setEnabled(false);
        dec.setEnabled(false);
        div.setEnabled(false);
        mul.setEnabled(false);

    }

    public void enableAll() {
        add.setEnabled(true);
        dec.setEnabled(true);
        div.setEnabled(true);
        mul.setEnabled(true);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("logme555","onSaveInstanceState func called");
        if (!firstOp.getText().toString().isEmpty()) {outState.putFloat("OP1",Float.valueOf(firstOp.getText().toString()));}
        if (!secondOp.getText().toString().isEmpty()) {outState.putFloat("OP2",Float.valueOf(secondOp.getText().toString()));}
        if (!result.getText().toString().isEmpty()) {outState.putFloat("RES",Float.valueOf(result.getText().toString()));}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = new Float(0.0);
        decimal_place=2;
        setContentView(R.layout.activity_main);
        Log.i("logme555","onCreate func called");
        firstOp = findViewById(R.id.firstop);
        secondOp = findViewById(R.id.secondop);
        result = findViewById(R.id.textView2);
        add = findViewById(R.id.button1);
        dec = findViewById(R.id.button2);
        div = findViewById(R.id.button3);
        mul = findViewById(R.id.button4);
       // this is using cookbook 3 anonym implementation
        findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstOp.setText("");
                secondOp.setText("");
            }
        });

        disableAll();
        firstOp.addTextChangedListener(listenOnChanges);
        secondOp.addTextChangedListener(listenOnChanges);
    }


    // This is the cookbook 1 method used to disable or enable op buttons
    private final TextWatcher listenOnChanges = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            enableAll();
        }

        public void afterTextChanged(Editable s) {
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                disableAll();
            } else {
                enableAll();
            }
        }
    };

    public void calc(View view) {
        Log.d("calculator","got into calc func with view:" + view.toString());
        try {
            String op = ((Button)view).getText().toString();
            if (firstOp.getText().toString().isEmpty() || secondOp.getText().toString().isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Must Fill Both Operands!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
            Float first = Float.valueOf(firstOp.getText().toString());
            Float second = Float.valueOf(secondOp.getText().toString());
            //String.format("%.2f", floatValue);
            if(op.equals("+")) {
                res = first + second;
            }else if(op.equals("-")) {
                res = first - second;
            } else if (op.equals("*")) {
                res = first * second;
            } else if (op.equals("/")) {
                if (second==0) {
                    Context context = getApplicationContext();
                    CharSequence text = "Don't Divide By Zero!!!";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                } else {
                    res = first / second;
                }
            }
            String resString = String.format("%." + decimal_place + "f\n", res);
            result.setText(resString);
        } catch (Exception e) {
            Context context = getApplicationContext();
            CharSequence text = "SOME ERROR!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            result.setText("ERROR");
        }
    }/////

    @Override
    public void onFinishEditDialog(int decimal_val) {
        decimal_place = decimal_val;
        Log.i("tibi","decimal value recieved!");
        String resString = String.format("%." + decimal_place + "f\n", res);
        result.setText(resString);
    }
}
