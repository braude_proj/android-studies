package com.example.ex6;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.SeekBar;
import android.widget.TextView;

public class MyDialogFragment extends DialogFragment implements SeekBar.OnSeekBarChangeListener , View.OnClickListener {

    private TextView mEditText;
    private SeekBar seekbar;
    private int decimal_place;
    public Float res;

    public MyDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                DialogListener listener = (DialogListener) getActivity();
                listener.onFinishEditDialog(decimal_place);
                // Close the dialog and return back to the parent activity
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    // 1. Defines the listener interface with a method passing back data result.
    public interface DialogListener {
        void onFinishEditDialog(int decimal_val);
    }


    public static MyDialogFragment newInstance(String title, int decimal_place) {
        MyDialogFragment frag = new MyDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.seekbar_dialog, container);
        res = new Float(128.0);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        mEditText = view.findViewById(R.id.example_value);
        seekbar = view.findViewById(R.id.seekBar);

        seekbar.setOnSeekBarChangeListener(this);
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        decimal_place = getArguments().getInt("decimal_val", 2);
        seekbar.setProgress(decimal_place);
        seekbar.setProgress(decimal_place);
        String resString = String.format("%." + decimal_place + "f\n", res);
        mEditText.setText(resString);
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        view.findViewById(R.id.ok).setOnClickListener(this);
        view.findViewById(R.id.cancel).setOnClickListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        decimal_place = progress;
        if (mEditText.getText().toString().isEmpty()) return;
        String resString = String.format("%." + decimal_place + "f\n", res);
        mEditText.setText(resString);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

}
