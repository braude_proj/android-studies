package com.example.ex10;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public TextView foreground;
    public TextView background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        foreground = findViewById(R.id.forground_counter);
        background = findViewById(R.id.background_counter);
        background.setText("100");
        if (!isMyServiceRunning(MyBackground.class)){
            Intent intent = new Intent(this, MyBackground.class);
            startService(intent);
            Log.i("tibi","started background service");
        } else {
            Log.i("tibi","Background service already running");
        }
        if (!isMyServiceRunning(MyForeground.class)){
            Intent intent = new Intent(this, MyForeground.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.i("tibi","started foreground service");
                //startForegroundService(intent);
            } else {
                Log.i("tibi","wont start service because of build version! its : " +  Build.VERSION.SDK_INT + " while needs to be " + Build.VERSION_CODES.O);
            }
        } else {
            Log.i("tibi","Foreground service already running");
        }


    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public class MyUiReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action  = intent.getAction();
            Log.i("tibi", "action on reciever" + action);
            int cur_val;
            switch (action){
                case "bg_actions":
                    cur_val = Integer.parseInt(background.getText().toString());
                    Log.i("tibi", "updating counter in background service" + cur_val);
                    if (cur_val>0) {
                        background.setText(cur_val - 1);
                    }
                    break;
                case "fg_actions":
                    cur_val = Integer.parseInt(foreground.getText().toString());
                    Log.i("tibi", "updating counter in foreground service" + cur_val);
                    if (cur_val>0) {
                        foreground.setText(cur_val - 1);
                    }
                    break;
            }
        }
    }
}