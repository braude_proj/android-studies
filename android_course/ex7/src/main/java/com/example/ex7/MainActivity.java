package com.example.ex7;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CountryXMLParser parser = new CountryXMLParser();
        final ListView list = (ListView) findViewById(R.id.country_list);

        ArrayList<Country> values = parser.parseCountries(getApplicationContext());

        final MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this,values);
        list.setAdapter(adapter);
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view,
                                           int position, long id) {
                final Country item = (Country) parent.getItemAtPosition(position);
                adapter.remove(item);
                return false;
            }
        });

    }

    public class MySimpleArrayAdapter extends ArrayAdapter<Country> {
        private final Context context;
        private final ArrayList<Country> values;

        public MySimpleArrayAdapter(Context context, ArrayList<Country> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_item, parent, false);
            TextView title =  rowView.findViewById(R.id.firstLine);
            TextView description = rowView.findViewById(R.id.secondLine);
            ImageView imageView = rowView.findViewById(R.id.icon);
            description.setText(values.get(position).getShorty());
            title.setText(values.get(position).getName());
            Resources resources = context.getResources();
            final int resourceId = resources.getIdentifier(values.get(position).getFlag(), "drawable",
                    context.getPackageName());

            imageView.setImageDrawable(resources.getDrawable(resourceId));

            return rowView;
        }
    }


}

