package com.example.ex1;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.tomerrosenfeld.customanalogclockview.CustomAnalogClock;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout1);
    }

    public void to2(View v)
    {
        setContentView(R.layout.layout2);
    }

    public void to3(View view) {
        setContentView(R.layout.layout3);
        CustomAnalogClock customAnalogClock = (CustomAnalogClock) findViewById(R.id.analog_clock);
        customAnalogClock.setAutoUpdate(true);
    }

    public void to4(View view) {
        setContentView(R.layout.layout4);
    }

    public void to1(View view) {
        setContentView(R.layout.layout1);
    }
}
