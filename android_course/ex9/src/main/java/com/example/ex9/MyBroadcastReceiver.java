package com.example.ex9;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private static String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static ConnectivityManager sConnectivityManager;
    String msgBody = "", phoneNo = "";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TiBi", "onReceive");
        if ( intent.getAction() == SMS_RECEIVED ) {
            Bundle databundle = intent.getExtras();
            if (databundle!=null) {
                Object mypdu[] = (Object[]) databundle.get("pdus");
                final SmsMessage[] msg = new SmsMessage[mypdu.length];
                for (int i = 0; i< mypdu.length ; i++) {
                    String format = databundle.getString("format");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        msg[i] = SmsMessage.createFromPdu((byte[]) mypdu[i], format);
                    }
                    msgBody = msg[i].getMessageBody();
                    phoneNo = msg[i].getOriginatingAddress();
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("PhoneNo: " + phoneNo + "\n");
            sb.append("msg body: " + msgBody + "\n");
            String log = sb.toString();

            Toast.makeText(context, log, Toast.LENGTH_LONG).show();
            Log.d("TiBi", log);
        }
        if ( intent.getAction() == ConnectivityManager.CONNECTIVITY_ACTION) {


            Bundle databundle = intent.getExtras();
            if (databundle!=null) {
                String msg = "No Network Connectivity to Internet";
                boolean networkIsActive = getNetworkState(context);
                if (networkIsActive) {
                    msg = "Network Is Connected to Internet";
                }
                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                Log.d("TiBi", "in connectivity action receive");
            }

        }

    }

    public static boolean getNetworkState(Context context){
        if(sConnectivityManager == null){
            sConnectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo activeNetworkInfo = sConnectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if (activeNetworkInfo.getType() == (ConnectivityManager.TYPE_MOBILE)) {
                return true;
            }
        }
        return false;
    }
}
