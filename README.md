# ex1 
## background & layouts
* layouts
* xmp files
* tables
* R
* using open source widgets 
---

# ex2 
## basic calculator 
* app lifesycle
* integration between code and layouts
* logging 
---

# Lab3
## communication between activitys (Intent)
## ex3a - registration app
## ex3b - duplicate of ex3a
## ex3client - go register with ex3a/ex3b 
* Intents 
* actions
* using dialer/email/web browser/registration 
---

# ex4 
## listeners & inflating  
* using different types of listeners
* reusing layouts (Inflating layouts)
* using seekbar with listener to interact with layout
---

# ex5
## fragments
* split of calculator to fragments
* fragment A for aperands and operation
* fragment B for result and seekbar
---

# ex6
## menus and dialogs
* adding menu to calculator
* settings menu option to open seekbar
---

# ex7 
## adapters & listview/gridview
* country list using adapter
* long click deletes list item
---